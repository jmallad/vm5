This project was an attempt at implementing swarm AI with a simple virtual
machine in a GLSL compute shader. The basic idea was that a host application
would load virtual programs into an array of buffers. The array would be
uploaded to the GPU, and a varied amount of "agents" would be spawned
(compute shader invocations). This project became extremely unwieldy very
fast; I learned the hard way why GLSL compute shaders are not more common.
There is no concept of modules; everything must be in a single large file.
This led me to split up my files anyway, and concatenate them during testing -
which led to a nightmare of debugging. A kind Redditor eventually introduced
me to the Rust GPU and Rust CUDA projects, and I intend to try this again
using those tools at a later point.
