#!/usr/bin/env bash
## zai_validate.sh - Assemble and pre-compile the ZAI shader

version="$(git log -1 --oneline | cut -d ' ' -f 1)"
out="zai_${version}.comp"
bin="zai_${version}.spv"

# First, append all snippets to a single GLSL file
buf=""
for snippet in "$(cat order.txt | sed '1d')"; do
    buf="${buf}$(cat ${snippet})"
done
echo "$buf" > "$out"

# Validate and compile the shader
glslangValidator -V "$out" -o "$bin" &> err.log

# Check for errors
lines=( )
d=0;
while read line; do
    lines[$d]="$line"
    ((d++))
done <<< "$(grep ERROR err.log)"
numbers="$(echo $lines | cut -d ':' -f 3 | grep -E '[0-9]+')"
d=0
if [ -z "$numbers" ]; then
    echo -e "\e[1;32mNo errors\e[0m"
    echo "GLSL output: $out"
    echo "SPIR-V output: $bin ($(du -b ${bin} | cut -d '	' -f 1) bytes)"
    exit
fi

# Report errors
for number in "$numbers"; do
    t="$number"
    for ((i=$number; i>=1; i--)); do
	module="$(sed -n "${i}p" "$out" | grep -E '^\*\* zai_[a-z]+.comp')"
	if [ ! -z "$module" ]; then
	    break;
	fi
    done
    module="${module#* }"
    module="${module%% *}"
    number="$((number - 5))"
    err="Error in module: \e[1;34m${module}\e[0m\n"
    err="${err}${lines[$d]}\n"
    for ((i=0; i < 5; i++)); do
	err="${err}$(sed -n "$((number + i))p" "$out")\n"
    done
    err="${err}\e[1;31m$(sed -n "$((number + 5))p" "$out")\e[0m\n"
    for ((i=6; i < 10; i++)); do
	err="${err}$(sed -n "$((number + i))p" "$out")\n"
    done
    echo -e "$err"
    ((d++))
done
